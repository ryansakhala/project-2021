import json
import os
from werkzeug.utils import secure_filename
from tensorflow.keras.models import Model , load_model
from tensorflow.keras.preprocessing import image
from flask import Flask, render_template, session, request, flash, redirect
import flask
import cv2
with open('config.json', 'r') as c:
    params = json.load(c)["params"]
import numpy as np
app = Flask(__name__, template_folder='template', static_folder='static')
#app.secret_key = 'super-secret-key'
#app.config['UPLOAD_FOLDER'] = params['upload_location']

Model= load_model('E:\Projects2021\Tomato Project\PROJECT\static\models\model_VGG19.h5')

def model_predict(img_path, model):
   # print(img_path)
    img = image.load_img(img_path, target_size=(224, 224))

    # Preprocessing the image
    x = image.img_to_array(img)
    # x = np.true_divide(x, 255)
    ## Scaling
    x=x/255.
    x = np.expand_dims(x, axis=0)
   

    # Be careful how your trained model deals with the input
    # otherwise, it won't make correct prediction!
   # x = preprocess_input(x)

    preds = model.predict(x)
    preds=np.argmax(preds, axis=1)
    if preds==0:
        preds="Bacterial_spot"
    elif preds==1:
        preds="Early_blight"
    elif preds==2:
        preds="Late_blight"
    elif preds==3:
        preds="Leaf_Mold"
    elif preds==4:
        preds="Septoria_leaf_spot"
    elif preds==5:
        preds="Spider_mites Two-spotted_spider_mite"
    elif preds==6:
        preds="Target_Spot"
    elif preds==7:
        preds="Tomato_Yellow_Leaf_Curl_Virus"
    elif preds==8:
        preds="Tomato_mosaic_virus"
    else:
        preds="Healthy"
        
    
    
    return preds

@app.route('/', methods=['GET', 'POST'])
def index():
    if "user" in session and session['user'] == params['admin_user']:
        return render_template('index.html', params=params)

    if request.method == "POST":
        username = request.form.get("uname")
        userpass = request.form.get("pass")
        if username == params['admin_user'] and userpass == params['admin_password']:
            # set the session variable
            session['user'] = username
            return render_template("index.html", params=params)
    return render_template('login.html', params=params)

    

@app.route("/uploader", methods=['GET', 'POST'])
def uploader():
    if request.method == "POST":
        f = request.files['file1']
        basepath=os.path.dirname(__file__)
        filepath=os.path.join(basepath,'uploaded_files',secure_filename(f.filename))
        f.save(filepath)
        #f.save(os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(f.filename)))
      
        #img_path=app.config['UPLOAD_FOLDER'],'/',f.filename
        #img_path=str(img_path[0])
        predict = model_predict(filepath, Model)
        
        return render_template('prediction.html',prediction_text='Biopsy {}'.format(predict))





app.run(debug=True)
